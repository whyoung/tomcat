导入原码到idea

1.新增 pom文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

  <modelVersion>4.0.0</modelVersion>
  <groupId>org.apache</groupId>
  <artifactId>tomcat</artifactId>
  <name>apache-tomcat-8.5.57</name>
  <version>8.5.57</version>

  <build>
    <finalName>Tomcat-8.5.57</finalName>
    <sourceDirectory>java</sourceDirectory>
    <testSourceDirectory>test</testSourceDirectory>
    <resources>
      <resource>
        <directory>java</directory>
      </resource>
    </resources>
    <testResources>
      <testResource>
        <directory>test</directory>
      </testResource>
    </testResources>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-compiler-plugin</artifactId>
        <version>3.5.1</version>
        <configuration>
          <encoding>UTF-8</encoding>
          <source>1.8</source>
          <target>1.8</target>
        </configuration>
      </plugin>
    </plugins>
  </build>

  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.easymock</groupId>
      <artifactId>easymock</artifactId>
      <version>3.4</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.apache.ant</groupId>
      <artifactId>ant</artifactId>
      <version>1.10.0</version>
    </dependency>
    <dependency>
      <groupId>wsdl4j</groupId>
      <artifactId>wsdl4j</artifactId>
      <version>1.6.2</version>
    </dependency>
    <dependency>
      <groupId>javax.xml</groupId>
      <artifactId>jaxrpc</artifactId>
      <version>1.1</version>
    </dependency>
    <dependency>
      <groupId>org.eclipse.jdt.core.compiler</groupId>
      <artifactId>ecj</artifactId>
      <version>4.6.1</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.glassfish/javax.xml.rpc -->
    <dependency>
      <groupId>org.glassfish</groupId>
      <artifactId>javax.xml.rpc</artifactId>
      <version>3.0.1-b03</version>
    </dependency>

    <!-- https://mvnrepository.com/artifact/biz.aQute.bnd/annotation -->
    <dependency>
      <groupId>biz.aQute.bnd</groupId>
      <artifactId>biz.aQute.bndlib</artifactId>
      <version>6.3.1</version>
      <scope>provided</scope>
    </dependency>

    <!-- https://mvnrepository.com/artifact/org.apache.tomcat/jakartaee-migration -->
    <dependency>
      <groupId>org.apache.tomcat</groupId>
      <artifactId>jakartaee-migration</artifactId>
      <version>1.0.0</version>
    </dependency>


  </dependencies>
</project>
```

具体的依赖需要取决于版本

在 `ContextConfig`类中添加 jsp 的解析器，否则会报空指针，加到`configureContext`方法中，其他地方也是可以的，只要能保证能够初始化

```java
context.addServletContainerInitializer(new JasperInitializer(), null);
```

注释掉 `JDTCompiler`中找不到的常量（如果编译不报错可以忽略）

增加启动参数

```
-Dcatalina.home=/Users/why/Public/code/tomcat
-Dcatalina.base=/Users/why/Public/code/tomcat
-Djava.endorsed.dirs/Users/why/Public/code/tomcat/endorsed
-Djava.io.tmpdir/Users/why/Public/code/tomcat/temp
-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager
-Djava.util.logging.config.file=/Users/why/Public/code/tomcat/conf/logging.properties
-Duser.language=en
-Duser.region=US
```